# i5-mspr-3d-ar

## Architecture des fichiers

```
- index.html    # contient le squelette de la page HTML ainsi que les différents contrôles
- package.js    # contient les informations du projet ainsi que les dépendances
- README.md
- js/           # contient les sources de l'applications
- css/          # contient les feuilles de style de l'application
```

## Guide de contribution

Une branche par (grosse feature). Merge request pour la mettre sur `preprod`. Puis quand on a quelquechose de correct on merge `preprod` sur `master`.
Merci de mettre des noms de branches et de commit "potables".